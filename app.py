"""
app.py:
    initializes and registers app components,
    loads configuration,
    sets up migration util
"""

# library imports
from flask import Flask, render_template
from flask_cors import CORS
from flask_heroku import Heroku

# file imports
from api.api import api
from api.data import getData, getUniqueData

# initialize flask app
app = Flask("CredibleToJson")
heroku = Heroku(app)

# Index page with README
@app.route("/", methods=["GET"])
def index():
    return render_template("index.html")

# load config from config file
app.config.from_object('config.Config')

# register api blueprint
app.register_blueprint(api, url_prefix="/api")


