"""
api.py:
    api routing
"""

# Library imports
from flask import Blueprint, jsonify, request

# File imports
from .data import getData, getUniqueData

api = Blueprint('api', __name__)


""" Get all data """
@api.route('/all/<string:connection>', methods=['GET'])
def all(connection):
    if connection == None or connection == "":
        return jsonify({
            "status": "error",
            "message": "Missing connection string"
        }), 404

    start = ""
    end = ""
    p1 = ""
    p2 = ""
    p3 = ""

    if "start" in request.args.keys():
        start = request.args["start"]
    if "end" in request.args.keys():
        end = request.args["end"]
    if "p1" in request.args.keys():
        p1 = request.args["p1"]
    if "p2" in request.args.keys():
        p2 = request.args["p2"]
    if "p3" in request.args.keys():
        p3 = request.args["p3"]

    try:
        return jsonify({
            "status": "success",
            "data": getData(connection, start, end, p1, p2, p3)
        }), 200
    except Exception:
        return jsonify({
            "status": "error",
            "message": "Something went wrong, check connection string and parameters"
        }), 500
    
    

""" Get unique data """
@api.route('/unique/<string:connection>/<string:key>', methods=['GET'])
def unique(connection, key):
    if connection == None or connection == "":
        return jsonify({
            "status": "error",
            "message": "Missing connection string"
        }), 404

    if key == None or key == "":
        return jsonify({
            "status": "error",
            "message": "Missing key for unique values"
        }), 404

    start = ""
    end = ""
    p1 = ""
    p2 = ""
    p3 = ""

    if "start" in request.args.keys():
        start = request.args["start"]
    if "end" in request.args.keys():
        end = request.args["end"]
    if "p1" in request.args.keys():
        p1 = request.args["p1"]
    if "p2" in request.args.keys():
        p2 = request.args["p2"]
    if "p3" in request.args.keys():
        p3 = request.args["p3"]

    try:
        return jsonify({
            "status": "success",
            "data": getUniqueData(connection, key, start, end, p1, p2, p3)
        }), 200
    except Exception:
        return jsonify({
            "status": "error",
            "message": "Something went wrong, check connection string and parameters"
        }), 500
