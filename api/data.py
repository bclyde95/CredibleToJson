import requests
import json
import xmltodict

def getData(connection, start='', end='', p1='', p2='', p3=''):
    # Query Credible database
    response = requests.get(f"https://reportservices.crediblebh.com/reports/ExportService.asmx/ExportXML?connection={connection}&start_date={start}&end_date={end}&custom_param1={p1}&custom_param2={p2}&custom_param3={p3}")

    # Strip stupid string wrap from response
    stringData = json.loads(json.dumps(xmltodict.parse(response.content.decode('utf-8'))))['string']

    # Waft through the unnecessary wrappers to get actual data in dicts. Remove None types along the way
    dataDict = list(filter(lambda x: x != None,xmltodict.parse(stringData['#text'])["NewDataSet"]["Table"]))

    return dataDict

def getUniqueData(connection, key, start='', end='', p1='', p2='', p3=''):
    data = getData(connection, start, end, p1, p2, p3)

    # Get only unique dicts from list of dicts
    return list({v[key]:v for v in data}.values())
    

def main():
    # Test with supervisor email list
    print(getUniqueData('LYEC1uwvr-7RAoxbT4TJDuiO!gY1p8-aFVdERsxbI0dA4iXyxzxzOVkIbcFQyHqC', 'supervisoremail', 'egahringer@kbbh.org'))


if __name__ == '__main__':
    main()
