# CredibleToJson
#### Start progress on easily getting usable JSON from Credible's webservice 

## /all/<connection>?params
Get all data

## /unique/<connection>/<unique-key>?params
Get only unique data

## Params
* start
* end
* p1
* p2
* p3
